const { merge } = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const vars = require('./vars');
const baseConfig = require('./webpack.config.base');

module.exports = merge(baseConfig, {
  mode: 'production',
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              // modules: true,
              sourceMap: true,
            },
          },
          'sass-loader',
        ],
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: '[name].[fullhash:20].[ext]',
              limit: 8192,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].[fullhash].css',
      chunkFilename: '[id].[fullhash].css',
    }),
    new HtmlWebpackPlugin({
      template: vars.indexHtmlSrcPath,
      title: vars.appName,
      currentYear: vars.currentYear,
      experienceYear: vars.experienceYear,
      appDescription: vars.appDescription,
      gtmId: vars.gtmId,
      nonce: vars.nonce,
      inject: 'body',
      chunks: ['main'],
      filename: 'index.html',
      minify: {
        collapseWhitespace: true,
        removeComments: true,
        removeRedundantAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true,
        useShortDoctype: true,
      },
    }),
    new FaviconsWebpackPlugin({
      logo: vars.faviconSrcPath,
      inject: true,
      cache: false,
      prefix: 'icons/',
      favicons: {
        appName: vars.appName,
        appDescription: vars.appDescription,
        developerName: 'Oleksii Volkov',
        developerURL: 'https://ovolkov.page',
        background: '#222629',
        theme_color: '#86c232',
        icons: {
          android: true,
          appleIcon: true,
          appleStartup: true,
          favicons: true,
          yandex: false,
          windows: true,
        },
      },
    }),
    // new CleanWebpackPlugin(),
    new CopyPlugin({
      patterns: [
        { from: 'src/robots.txt', to: 'robots.txt' },
        { from: 'src/sitemap.xml', to: 'sitemap.xml' },
        { from: 'src/.well-known', to: '.well-known' },
        { from: 'src/.well-known/googlecad96540dfb6d7c4.html', to: 'googlecad96540dfb6d7c4.html' },
        { from: 'src/.well-known/keybase.txt', to: 'keybase.txt' },
        { from: 'src/.well-known/pinterest-e2b58.html', to: 'pinterest-e2b58.html' },
        { from: 'src/.well-known/security.txt', to: 'security.txt' },
      ],
    }),
  ],
  optimization: {
    splitChunks: {
      cacheGroups: {
        styles: {
          name: 'styles',
          test: /\.css$/,
          chunks: 'all',
          enforce: true,
        },
      },
    },
    minimizer: [
      new TerserPlugin({
        // cache: true,
        // sourceMap: true,
        parallel: true,
        terserOptions: {
          ecma: 6,
        },
      }),
      new CssMinimizerPlugin({}),
    ],
  },
});
