const path = require('path');
require('dotenv').config();

const vars = {
  appName: 'Oleksii Volkov | Senior Software Engineer',
  appDescription: 'Personal site of Oleksii Volkov - Senior Software Engineer',
  gtmId: process.env.GTM_ID,
  contextPath: path.resolve(__dirname),
  buildPath: path.resolve(__dirname, 'dist'),
  srcPath: path.resolve(__dirname, 'src'),
  nonce: 'YXRkMkJMeThzVjk0ZThLVDQ4Sm5xTUNpb1hXejYzb3k3dEd1SlRSWTd3bkY=',
};

vars.currentYear = (new Date()).getFullYear();
vars.experienceYear = (new Date()).getFullYear() - 2011;
vars.indexHtmlSrcPath = path.resolve(vars.srcPath, 'index.ejs');
vars.mainJsSrcPath = path.resolve(vars.srcPath, 'main.js');
vars.faviconSrcPath = path.resolve(vars.srcPath, 'images', 'logo.png');

module.exports = vars;
