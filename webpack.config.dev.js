const { merge } = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const vars = require('./vars');
const baseConfig = require('./webpack.config.base.js');

module.exports = merge(baseConfig, {
  mode: 'development',
  devtool: 'eval-cheap-module-source-map',
  devServer: {
    https: false,
    hot: true,
    static: vars.srcPath,
    devMiddleware: {
      writeToDisk: true,
    }
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: '[path][name].[ext]?fullhash=[fullhash:20]',
              limit: 8192,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: vars.indexHtmlSrcPath,
      title: vars.appName,
      currentYear: vars.currentYear,
      experienceYear: vars.experienceYear,
      appDescription: vars.appDescription,
      nonce: 'YXRkMkJMeThzVjk0ZThLVDQ4Sm5xTUNpb1hXejYzb3k3dEd1SlRSWTd3bkY=',
      inject: true,
      chunks: ['main'],
      filename: 'index.html',
    }),
  ],
});
