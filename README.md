# Development

> npm i

> npm start

# Production

1) Copy [.env.dist](.env.dist) as [.env](.env)
2) Replace "GTM-XXXXXXX" by real value from https://tagmanager.google.com/
3) > npm run update:production
