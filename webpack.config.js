const path = require('path');

module.exports = env => require(path.resolve(__dirname, `webpack.config.${env.prod ? 'prod' : 'dev'}.js`));
