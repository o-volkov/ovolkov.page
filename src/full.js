require('./styles/_base.scss');
require('./styles/main.scss');

const checkNavMenu = () => {
  const home = document.getElementById('home');
  const navMenu = document.getElementsByClassName('nav-menu').item(0);

  if (window.scrollY > home.clientHeight - navMenu.clientHeight) {
    navMenu.classList.add('sticky');
  } else {
    navMenu.classList.remove('sticky');
  }
};

document.onscroll = _ => {
  checkNavMenu();
};

(function() {
  checkNavMenu();
})();
