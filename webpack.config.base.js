const vars = require('./vars');

module.exports = {
  target: 'web',
  mode: 'none',
  // context: vars.contextPath,
  entry: [
    vars.mainJsSrcPath,
  ],
  resolve: { extensions: ['.js'] },
  output: {
    filename: '[name].[fullhash:20].js',
    path: vars.buildPath,
    clean: true,
  },
  module: {
    rules: [
      {
        test: /\.(svg|gif|jpg|png|eot|woff|ttf)$/,
        use: [
          'url-loader',
        ],
      },
    ],
  },
};
